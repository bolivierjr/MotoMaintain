import os
from config import app_config
from flask import Flask, render_template


def create_app():
    app = Flask(__name__,
                static_folder="../../dist/static",
                template_folder="../../dist")

    app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')
    print(app.config)
    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>')
    def catch_all(path):
        return render_template("index.html")

    return app
